export const GENERAL_VALUE: any = {
    Y: 'Y',
    N: 'N',
    S: 'S',
    OP: 'OP',
    PAGE_SIZE_OPTIONS: [5, 10, 25, 100],
    ACCEPT_FILES: ['.jpg', '.jpeg', '.png']
};
