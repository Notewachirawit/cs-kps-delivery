import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { AuthenticationService } from './../../_centralized/_services/authentication.service';
import { RealtimeDatabaseService } from './../../_centralized/_services/realtime-database.service';

import { Profile } from './../../_centralized/_models/_order-item/profile';

import { GENERAL_VALUE } from './../../_centralized/_configuration/general-value';
import { PATH_PAGES } from './../../_centralized/_configuration/path-pages';

@Component({
  selector: 'cs-kps-delivery-staff-form',
  templateUrl: './staff-form.component.html',
  styleUrls: ['./staff-form.component.scss']
})
export class StaffFormComponent implements OnInit {

  staffForm: FormGroup;
  profile: Profile;
  uid: string;
  statusSpinner: boolean;
  passwordVisibilityIcon: boolean;
  confirmPasswordVisibilityIcon: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private realtimeDatabaseService: RealtimeDatabaseService
  ) { }

  ngOnInit(): void {
    this.staffForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      confirmPassword: [null, [Validators.required]],
      name: [null, [Validators.required]],
      address: [null, [Validators.required]],
      telephone: [null, [Validators.required]]
    });
    this.passwordVisibilityIcon = true;
    this.confirmPasswordVisibilityIcon = true;
    setTimeout(() => {
      this.statusSpinner = true;
      this.activatedRoute.queryParams.subscribe((params: any) => {
        console.log('..:: queryParams: ', params);
        if (params && '' !== params[PATH_PAGES.QUERY_PARAMS.KEY]) {
          this.uid = params[PATH_PAGES.QUERY_PARAMS.KEY];
          const readOnce: Subscription = this.realtimeDatabaseService.getUsersByKey(this.uid)
            .subscribe((resultsUser: any) => {
              const resultsValueUser: any = resultsUser.payload.val();
              console.log('.. :: Results Value Menu: ', resultsValueUser);
              readOnce.unsubscribe();
              if (resultsValueUser) {
                this.profile = resultsValueUser.profile;
                this.staffForm = this.formBuilder.group({
                  username: [null, [Validators.required]],
                  name: [null, [Validators.required]],
                  address: [null, [Validators.required]],
                  telephone: [null, [Validators.required]]
                });
                this.staffForm.patchValue({
                  username: this.profile.username,
                  name: this.profile.name,
                  address: this.profile.address,
                  telephone: this.profile.telephone
                });
                this.staffForm.controls['username'].disable();
              }
              this.statusSpinner = false;
            }, this.onServiceError.bind(this));
        } else {
          this.statusSpinner = false;
        }
      });
    }, 0);
  }

  private onServiceError(error: any): void {
    this.statusSpinner = false;
    this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    console.log('..:: Service Error: ', error);
  }

  private createUser(username: string, password: string, profile: Profile): void {
    this.statusSpinner = true;
    this.authenticationService
      .createUserWithUsernameAndPassword(username, password)
      .then((userCredential: firebase.auth.UserCredential) => {
        const objectCreateUser: object = {};
        console.log('UID : ', userCredential.user.uid);
        objectCreateUser[userCredential.user.uid] = {
          profile: profile
        };
        console.log('Object Create User : ', objectCreateUser);
        this.saveInformationUser(objectCreateUser);
        this.statusSpinner = false;
      }).catch(this.onServiceError.bind(this));
  }

  private updateUser(profile: Profile): void {
    this.statusSpinner = true;
    const objectUpdateUser: object = {};
    console.log('UID : ', this.uid);
    objectUpdateUser[this.uid] = {
      profile: profile
    };
    console.log('Object Update User : ', objectUpdateUser);
    this.saveInformationUser(objectUpdateUser);
    this.statusSpinner = false;
  }

  private saveInformationUser(user: object): void {
    this.realtimeDatabaseService.saveInformationUser(user).then(() => {
      console.log('Save Information User Success: ', user);
      this.routerNavigateToStaff();
    }).catch(this.onServiceError.bind(this));
  }

  onFormSubmit(): void {
    console.log('.. :: Form Submit Valid: ', this.staffForm);
    if (this.staffForm.valid) {
      if (!this.profile) {
        if (this.staffForm.controls.password.value === this.staffForm.controls.confirmPassword.value) {
          this.createUser(
            this.staffForm.controls.username.value,
            this.staffForm.controls.password.value,
            {
              username: this.staffForm.controls.username.value,
              address: this.staffForm.controls.address.value,
              name: this.staffForm.controls.name.value,
              telephone: this.staffForm.controls.telephone.value,
              userType: GENERAL_VALUE.S
            }
          );
        }
      } else if (this.profile) {
        this.profile = {
          username: this.staffForm.getRawValue().username,
          address: this.staffForm.controls.address.value,
          name: this.staffForm.controls.name.value,
          telephone: this.staffForm.controls.telephone.value,
          userType: GENERAL_VALUE.S
        };
        this.updateUser(this.profile);
      }
    }
  }

  // deleteUserItem(): void {

  // }

  routerNavigateToStaff(): void {
    this.staffForm.reset();
    this.router.navigate(['/' + PATH_PAGES.AUTHENTICATE.ROOT + '/' + PATH_PAGES.AUTHENTICATE.STAFF], {});
  }

}
