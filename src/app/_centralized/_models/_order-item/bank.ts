export interface Bank {
    bankName: string;
    bankNo: string;
    bankOwner: string;
}
