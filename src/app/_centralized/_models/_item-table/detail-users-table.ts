import { Profile } from '../_order-item/profile';

export interface DetailUserTable {
    uid: string;
    username: string;
    name: string;
    address: string;
    telephone: string;
    profile: Profile;
}
