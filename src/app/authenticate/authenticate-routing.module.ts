import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthorizationGuard } from '../_centralized/_guards/authorization.guard';

import { AuthenticateComponent } from './authenticate.component';
import { FoodComponent } from './food/food.component';
import { FoodFormComponent } from './food-form/food-form.component';
import { HomeComponent } from './home/home.component';
import { HomeFormComponent } from './home-form/home-form.component';
import { SellingComponent } from './selling/selling.component';
import { StaffComponent } from './staff/staff.component';
import { StaffFormComponent } from './staff-form/staff-form.component';

import { PATH_PAGES } from '../_centralized/_configuration/path-pages';

const routes: Routes = [
  {
    path: PATH_PAGES.AUTHENTICATE.ROOT,
    component: AuthenticateComponent,
    children: [
      {
        path: PATH_PAGES.AUTHENTICATE.HOME,
        component: HomeComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: PATH_PAGES.AUTHENTICATE.HOME_FORM,
        component: HomeFormComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: PATH_PAGES.AUTHENTICATE.SELLING,
        component: SellingComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: PATH_PAGES.AUTHENTICATE.FOOD,
        component: FoodComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: PATH_PAGES.AUTHENTICATE.FOOD_FORM,
        component: FoodFormComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: PATH_PAGES.AUTHENTICATE.STAFF,
        component: StaffComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: PATH_PAGES.AUTHENTICATE.STAFF_FORM,
        component: StaffFormComponent,
        canActivate: [AuthorizationGuard]
      },
      {
        path: '',
        redirectTo: PATH_PAGES.AUTHENTICATE.HOME,
        pathMatch: PATH_PAGES.GENERAL.FULL
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticateRoutingModule { }
