import { KEYS_TRANSLATE } from './key-translate';
import { PATH_PAGES } from './path-pages';

export const OBJECTS_ROUTER_VIEW: any = {
    AUTHENTICATE: {
        ROOT: {
            MENU: [
                {
                    TEXT: KEYS_TRANSLATE.AUTHENTICATE.ROOT.HOME,
                    ROUTER: '/' + PATH_PAGES.AUTHENTICATE.ROOT + '/' + PATH_PAGES.AUTHENTICATE.HOME
                },
                {
                    TEXT: KEYS_TRANSLATE.AUTHENTICATE.ROOT.SELLING,
                    ROUTER: '/' + PATH_PAGES.AUTHENTICATE.ROOT + '/' + PATH_PAGES.AUTHENTICATE.SELLING
                },
                {
                    TEXT: KEYS_TRANSLATE.AUTHENTICATE.ROOT.FOOD,
                    ROUTER: '/' + PATH_PAGES.AUTHENTICATE.ROOT + '/' + PATH_PAGES.AUTHENTICATE.FOOD
                },
                {
                    TEXT: KEYS_TRANSLATE.AUTHENTICATE.ROOT.STAFF,
                    ROUTER: '/' + PATH_PAGES.AUTHENTICATE.ROOT + '/' + PATH_PAGES.AUTHENTICATE.STAFF
                }
            ]
        }
    }
};
