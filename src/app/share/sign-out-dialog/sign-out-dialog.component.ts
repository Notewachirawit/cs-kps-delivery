import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { GENERAL_VALUE } from './../../_centralized/_configuration/general-value';
import { PATH_PAGES } from './../../_centralized/_configuration/path-pages';

@Component({
  selector: 'cs-kps-delivery-sign-out-dialog',
  template: `
    <h1 mat-dialog-title>{{ 'SHARE.DO_YOU_WANT_TO_SIGN_OUT' | translate }}</h1>
    <div mat-dialog-actions>
      <button mat-button (click)="dialogClose('1')">{{ 'SHARE.NO' | translate }}</button>
      <span class="sidenav-spacer"></span>
      <button mat-button (click)="dialogClose('0')">{{ 'SHARE.YES' | translate }}</button>
    </div>
  `
})
export class SignOutDialogComponent implements OnInit {

  dialogResultList: string[];

  constructor(
    public signOutDialogRef: MatDialogRef<SignOutDialogComponent>
  ) { }

  ngOnInit(): void {
    this.dialogResultList = [GENERAL_VALUE.Y, GENERAL_VALUE.N];
  }

  dialogClose(dialogResultListIndex: string): void {
    const value: object = {};
    value[PATH_PAGES.DIALOG_REF.VALUE] = this.dialogResultList[dialogResultListIndex];
    this.signOutDialogRef.close(value);
  }


}
