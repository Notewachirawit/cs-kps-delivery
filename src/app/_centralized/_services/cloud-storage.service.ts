import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class CloudStorageService {

  constructor(
    private angularFireStorage: AngularFireStorage
  ) { }


  storageReference(pathFileName: string): AngularFireStorageReference {
    return this.angularFireStorage.ref(pathFileName);
  }

  uploadFile(pathFileName: string, blobFile: Blob): AngularFireUploadTask {
    return this.angularFireStorage.upload(pathFileName, blobFile);
  }

  deleteFile(pathFileName: string): Observable<any> {
    return this.angularFireStorage.ref(pathFileName).delete();
  }

}
