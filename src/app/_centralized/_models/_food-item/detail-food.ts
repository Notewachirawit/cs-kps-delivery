import { DetailImage } from '../_order-item/detail-image';

export interface DetailFood {
    price: number;
    unit: string;
    useFlag: boolean;
    detailImage: DetailImage;
    outOfStock: boolean;
}
