export const FIREBASE_PRODUCTS: any = {
    VERIFY_KEY: {
        apiKey: 'AIzaSyBSwORLYR9xOsWcSiZ-se1xtHl4vcO2aYw',
        authDomain: 'cskps-delivery.firebaseapp.com"',
        databaseURL: 'https://cskps-delivery.firebaseio.com',
        projectId: 'cskps-delivery',
        storageBucket: 'cskps-delivery.appspot.com',
        messagingSenderId: '709785654778'
    },
    PATH_RESULT: {
        MENU_ITEM: 'menuItem',
        ORDER_ITEM: 'orderItem',
        STATUS: 'status',
        USERS: 'users'
    },
    PATH_FILES: {
        MENU_ITEM: 'menu/item/'
    }
};
