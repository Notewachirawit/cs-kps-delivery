import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { MatPaginatorIntl } from '@angular/material';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AppRoutingModule } from './app-routing.module';
import { AuthenticateModule } from './authenticate/authenticate.module';
import { UnauthenticatedModule } from './unauthenticated/unauthenticated.module';

import { AuthenticationService } from './_centralized/_services/authentication.service';
import { CloudStorageService } from './_centralized/_services/cloud-storage.service';
import { ManageLocalStorageService } from './_centralized/_services/manage-local-storage.service';
import { MatPaginatorIntlCustomService } from './_centralized/_services/mat-paginator-intl-custom.service';
import { RealtimeDatabaseService } from './_centralized/_services/realtime-database.service';
import { AuthorizationGuard } from './_centralized/_guards/authorization.guard';

import { AppComponent } from './app.component';

import { COMMON_CENTER } from './_centralized/_configuration/common-center';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    UnauthenticatedModule,
    AuthenticateModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot(COMMON_CENTER.TRANSLATE_FUNCTION)
  ],
  providers: [
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlCustomService },
    AuthenticationService,
    CloudStorageService,
    ManageLocalStorageService,
    RealtimeDatabaseService,
    AuthorizationGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
