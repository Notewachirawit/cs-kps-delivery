import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { RealtimeDatabaseService } from './../../_centralized/_services/realtime-database.service';

import { DetailUserTable } from './../../_centralized/_models/_item-table/detail-users-table';

import { GENERAL_VALUE } from './../../_centralized/_configuration/general-value';
import { PATH_PAGES } from './../../_centralized/_configuration/path-pages';

@Component({
  selector: 'cs-kps-delivery-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  private _readOnce: Subscription;

  statusSpinner: boolean;
  pageSizeOptions: number[];
  dataSource: MatTableDataSource<DetailUserTable>;
  columnsToDisplay: string[];
  statusObject: any;
  keyTranslate: any;

  constructor(
    private router: Router,
    private realtimeDatabaseService: RealtimeDatabaseService
  ) { }

  ngOnInit() {
    this.pageSizeOptions = GENERAL_VALUE.PAGE_SIZE_OPTIONS;
    this.columnsToDisplay = ['username', 'name', 'address', 'telephone'];
    this.keyTranslate = {
      username: 'UNAUTHENTICATED.SIGN_IN.USERNAME',
      name: 'SHARE.NAME',
      address: 'SHARE.ADDRESS',
      telephone: 'SHARE.TELEPHONE'
    };

    setTimeout(() => {
      this.statusSpinner = true;
      this._readOnce = this.realtimeDatabaseService.getUsers().subscribe((resultsUsers: any) => {
        const resultsValueUsers: any = resultsUsers.payload.val();
        console.log('..:: Results Value Users: ', resultsValueUsers);
        if (resultsValueUsers) {
          const resultKeyUsers: string[] = Object.keys(resultsValueUsers);
          const detailUserTable: DetailUserTable[] = [];
          for (const key of resultKeyUsers) {
            if ('S' === resultsValueUsers[key].profile.userType) {
              detailUserTable.push({
                uid: key,
                username: resultsValueUsers[key].profile.username,
                name: resultsValueUsers[key].profile.name,
                address: resultsValueUsers[key].profile.address,
                telephone: resultsValueUsers[key].profile.telephone,
                profile: resultsValueUsers[key].profile
              });
              console.log(resultsValueUsers[key]);
            }
          }
          console.log('..:: Detail Users Table: ', detailUserTable);
          this.dataSource = new MatTableDataSource(detailUserTable);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
        this.statusSpinner = false;
      }, this.onServiceError.bind(this));
    }, 0);
  }

  ngOnDestroy(): void {
    this._readOnce.unsubscribe();
  }

  private onServiceError(error: any): void {
    this.statusSpinner = false;
    this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    console.log('..:: Service Error: ', error);
  }

  viewElement(element: DetailUserTable): void {
    console.log('Detail User: ', element);
    this.routerNavigateToStaffForm(element.uid);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  routerNavigateToStaffForm(keyDetailUser: string): void {
    const key: object = {};
    key[PATH_PAGES.QUERY_PARAMS.KEY] = keyDetailUser;
    this.router.navigate(['/' + PATH_PAGES.AUTHENTICATE.ROOT + '/' + PATH_PAGES.AUTHENTICATE.STAFF_FORM],
      {
        queryParams: key
      }
    );
  }

}
