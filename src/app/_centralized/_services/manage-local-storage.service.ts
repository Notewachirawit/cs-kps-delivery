import { KEY_LOCAL_STORAGE } from '../_configuration/key-local-storage';
import { Injectable } from '@angular/core';

@Injectable()
export class ManageLocalStorageService {

  getAuthenticate(): string {
    return localStorage.getItem(KEY_LOCAL_STORAGE.AUTHENTICATE) || KEY_LOCAL_STORAGE.STATUS_AUTHENTICATE.N;
  }

  setAuthenticate(status: string): void {
    localStorage.setItem(KEY_LOCAL_STORAGE.AUTHENTICATE, status);
  }

  getUid(): string {
    return localStorage.getItem(KEY_LOCAL_STORAGE.UID);
  }

  setUid(uid: string): void {
    localStorage.setItem(KEY_LOCAL_STORAGE.UID, uid);
  }

  removeUid(): void {
    localStorage.removeItem(KEY_LOCAL_STORAGE.UID);
  }

  setStatus(status: string): void {
    localStorage.setItem(KEY_LOCAL_STORAGE.STATUS, status);
  }

  getStatus(): string {
    return localStorage.getItem(KEY_LOCAL_STORAGE.STATUS);
  }

  removeStatus(): void {
    localStorage.removeItem(KEY_LOCAL_STORAGE.STATUS);
  }

}
