export const PATH_PAGES: any = {
    AUTHENTICATE: {
        ROOT: 'authenticate',
        HOME: 'home',
        HOME_FORM: 'home-form',
        SELLING: 'selling',
        FOOD: 'food',
        FOOD_FORM: 'food-form',
        STAFF: 'staff',
        STAFF_FORM: 'staff-form'
    },
    UNAUTHENTICATED: {
        ROOT: 'unauthenticated',
        SIGN_IN: 'sign-in'
    },
    GENERAL: {
        FULL: 'full'
    },
    QUERY_PARAMS: {
        ROUTER: 'router',
        KEY: 'KEY'
    },
    DIALOG_REF: {
        VALUE: 'value'
    }
};
