import { AngularFireDatabase, SnapshotAction } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { FIREBASE_PRODUCTS } from '../_configuration/firebase-products';

@Injectable()
export class RealtimeDatabaseService {

  constructor(private angularFireDatabase: AngularFireDatabase) { }

  getMenuItem(): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.MENU_ITEM).snapshotChanges();
  }

  getMenuItemByKey(key: string): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.MENU_ITEM + `/${key}`).snapshotChanges();
  }

  getOrderItem(): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.ORDER_ITEM).snapshotChanges();
  }

  getOrderItemByKey(key: string): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.ORDER_ITEM + `/${key}`).snapshotChanges();
  }

  saveOrUpdateOrderItem(orderItem: object): Promise<void> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.ORDER_ITEM).update(orderItem);
  }

  saveOrderItem(orderItem: object): Promise<void> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.ORDER_ITEM).update(orderItem);
  }

  getStatus(): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.STATUS).snapshotChanges();
  }

  saveOrUpdateMenuItem(dessertItem: object): Promise<void> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.MENU_ITEM).update(dessertItem);
  }

  deleteMenuItem(key: string): Promise<void> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.MENU_ITEM + `/${key}`).remove();
  }

  getUsers(): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.USERS).snapshotChanges();
  }

  getUsersByKey(key: string): Observable<SnapshotAction<any>> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.USERS + `/${key}`).snapshotChanges();
  }

  saveInformationUser(user: object): Promise<void> {
    return this.angularFireDatabase.object(FIREBASE_PRODUCTS.PATH_RESULT.USERS).update(user);
  }

}
