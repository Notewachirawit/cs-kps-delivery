import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PATH_PAGES } from './_centralized/_configuration/path-pages';

const routes: Routes = [
  {
    path: PATH_PAGES.UNAUTHENTICATED.ROOT,
    loadChildren: './unauthenticated/unauthenticated.module#UnauthenticatedModule'
  },
  {
    path: PATH_PAGES.AUTHENTICATE.ROOT,
    loadChildren: './authenticate/authenticate.module#AuthenticateModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
