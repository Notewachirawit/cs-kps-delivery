import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../_services/authentication.service';

import { PATH_PAGES } from './../_configuration/path-pages';

@Injectable()
export class AuthorizationGuard implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    console.log('.. :: Activated Route Snapshot :', next);
    console.log('.. :: Router State Snapshot :', state);
    if (this.authenticationService.isAuthenticate()) {
      return true;
    }
    this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    return false;
  }

}
