import { Subscription } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AuthenticationService } from './../../_centralized/_services/authentication.service';
import { ManageLocalStorageService } from './../../_centralized/_services/manage-local-storage.service';
import { RealtimeDatabaseService } from './../../_centralized/_services/realtime-database.service';

import { environment } from './../../../environments/environment';

import { KEY_LOCAL_STORAGE } from './../../_centralized/_configuration/key-local-storage';
import { KEYS_TRANSLATE } from './../../_centralized/_configuration/key-translate';
import { PATH_FILES } from './../../_centralized/_configuration/path-files';
import { PATH_PAGES } from './../../_centralized/_configuration/path-pages';
import { RECAPTCHA_PRODUCTS } from './../../_centralized/_configuration/recaptcha-products';

@Component({
  selector: 'cs-kps-delivery-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  signInForm: FormGroup;
  statusSpinner: boolean;
  passwordVisibilityIcon: boolean;
  useGlobalDomain: boolean;

  _KEYS_TRANSLATE_SHARE: any;
  _KEYS_TRANSLATE: any;
  _PATH_FILES: any;
  _RECAPTCHA_PRODUCTS: any;

  constructor(
    public translateService: TranslateService,
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private manageLocalStorageService: ManageLocalStorageService,
    private realtimeDatabaseService: RealtimeDatabaseService
  ) { }

  ngOnInit(): void {
    this.signInForm = this.formBuilder.group({
      username: [environment.production ? null : 'superadmin', [Validators.required]],
      password: [environment.production ? null : 'Admin2018', [Validators.required]],
      recaptcha2: [null, (environment.production ? [Validators.required] : [])]
    });
    this.statusSpinner = false;
    this.passwordVisibilityIcon = true;
    this._KEYS_TRANSLATE_SHARE = KEYS_TRANSLATE.SHARE;
    this._KEYS_TRANSLATE = KEYS_TRANSLATE.UNAUTHENTICATED.SIGN_IN;
    this._PATH_FILES = PATH_FILES.UNAUTHENTICATED.SIGN_IN;
    this._RECAPTCHA_PRODUCTS = RECAPTCHA_PRODUCTS;
    this.useGlobalDomain = environment.production;
  }

  private onServiceError(error: any): void {
    this.statusSpinner = false;
    this.manageLocalStorageService.removeStatus();
    this.manageLocalStorageService.removeUid();
    this.manageLocalStorageService.setAuthenticate(KEY_LOCAL_STORAGE.STATUS_AUTHENTICATE.N);
    console.log('..:: Service Error: ', error);
  }

  onFormSubmit(): void {
    console.log('.. :: Form Submit Valid: ', this.signInForm.valid);
    if (this.signInForm.valid) {
      this.statusSpinner = true;
      this.authenticationService
        .signInWithUsernameAndPassword(this.signInForm.value.username, this.signInForm.value.password)
        .then((userCredential: firebase.auth.UserCredential) => {
          console.log('..:: User Credential: ', userCredential);
          const readOnce: Subscription = this.realtimeDatabaseService
            .getUsersByKey(userCredential.user.uid).subscribe((resultsUser: any) => {
              const resultsValueUser: any = resultsUser.payload.val();
              console.log('.. :: Results Value Menu: ', resultsValueUser);
              readOnce.unsubscribe();
              if (!resultsValueUser) {
                this.manageLocalStorageService.setAuthenticate(KEY_LOCAL_STORAGE.STATUS_AUTHENTICATE.Y);
                this.manageLocalStorageService.setUid(userCredential.user.uid);
                this.statusSpinner = false;
                const queryParams: object = {};
                queryParams[PATH_PAGES.QUERY_PARAMS.ROUTER] = PATH_PAGES.UNAUTHENTICATED.SIGN_IN;
                this.router.navigate([PATH_PAGES.AUTHENTICATE.ROOT], {
                  queryParams: queryParams
                });
              } else {
                this.authenticationService.signOut()
                  .then(() => {
                    this.statusSpinner = false;
                    this.manageLocalStorageService.removeStatus();
                    this.manageLocalStorageService.removeUid();
                    this.manageLocalStorageService.setAuthenticate(KEY_LOCAL_STORAGE.STATUS_AUTHENTICATE.N);
                  })
                  .catch(this.onServiceError.bind(this));
              }
              this.statusSpinner = false;
            }, this.onServiceError.bind(this));

        }).catch(this.onServiceError.bind(this));
    }
  }

}
