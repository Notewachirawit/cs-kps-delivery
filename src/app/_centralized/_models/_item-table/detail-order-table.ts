import { DetailOrder } from '../_order-item/detail-order';

export interface DetailOrderTable {
    invoiceNumber: string;
    dateTime: string;
    name: string;
    totalPrice: number;
    status: string;
    detailOrder: DetailOrder;
}
