export interface ListOrder {
    name: string;
    amount: number;
}
