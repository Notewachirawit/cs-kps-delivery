import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { RealtimeDatabaseService } from './../../_centralized/_services/realtime-database.service';

import { DetailFoodTable } from './../../_centralized/_models/_item-table/detail-food-table';

import { GENERAL_VALUE } from './../../_centralized/_configuration/general-value';
import { PATH_PAGES } from './../../_centralized/_configuration/path-pages';

@Component({
  selector: 'cs-kps-delivery-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss']
})
export class FoodComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  private readOnce: Subscription;

  statusSpinner: boolean;
  pageSizeOptions: number[];
  detailFoodTable: DetailFoodTable[];
  dataSource: MatTableDataSource<DetailFoodTable>;
  columnsToDisplay: string[];
  statusObject: any;
  keyTranslate: any;

  constructor(
    private router: Router,
    private realtimeDatabaseService: RealtimeDatabaseService
  ) { }

  ngOnInit() {
    this.pageSizeOptions = GENERAL_VALUE.PAGE_SIZE_OPTIONS;
    this.columnsToDisplay = ['name', 'price', 'unit', 'imageFile', 'status'];
    this.keyTranslate = {
      name: 'SHARE.NAME',
      price: 'SHARE.PRICE',
      unit: 'SHARE.UNIT',
      imageFile: 'SHARE.IMAGE_FILE',
      status: 'SHARE.STATUS'
    };
    setTimeout(() => {
      this.statusSpinner = true;
      this.readOnce = this.realtimeDatabaseService.getMenuItem().subscribe((resultsMenuItem: any) => {
        const resultsValueMenuItem: any = resultsMenuItem.payload.val();
        console.log('..:: Results Value Menu Item: ', resultsValueMenuItem);
        if (resultsValueMenuItem) {
          const resultsKeyDessertItem: string[] = Object.keys(resultsValueMenuItem);
          this.detailFoodTable = [];
          for (const key of resultsKeyDessertItem) {
            this.detailFoodTable.push({
              name: key,
              price: resultsValueMenuItem[key].price,
              unit: resultsValueMenuItem[key].unit,
              imageFile: resultsValueMenuItem[key].detailImage.url,
              detailFood: resultsValueMenuItem[key],
              status: resultsValueMenuItem[key].outOfStock
            });
          }
          console.log('..:: Menu: ', this.detailFoodTable);
          this.dataSource = new MatTableDataSource(this.detailFoodTable);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
        this.statusSpinner = false;
      }, this.onServiceError.bind(this));
    }, 0);
  }

  ngOnDestroy(): void {
    this.readOnce.unsubscribe();
  }

  private onServiceError(error: any): void {
    this.statusSpinner = false;
    this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    console.log('..:: Service Error: ', error);
  }

  viewElement(element: DetailFoodTable): void {
    console.log('Detail Food: ', element);
    this.routerNavigateToFoodForm(element.name);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  routerNavigateToFoodForm(keyDetailFood: string): void {
    const key: object = {};
    key[PATH_PAGES.QUERY_PARAMS.KEY] = keyDetailFood;
    this.router.navigate(['/' + PATH_PAGES.AUTHENTICATE.ROOT + '/' + PATH_PAGES.AUTHENTICATE.FOOD_FORM],
      {
        queryParams: key
      }
    );
  }

}
