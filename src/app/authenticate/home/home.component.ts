import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ManageLocalStorageService } from './../../_centralized/_services/manage-local-storage.service';
import { RealtimeDatabaseService } from './../../_centralized/_services/realtime-database.service';

import { DetailOrderTable } from './../../_centralized/_models/_item-table/detail-order-table';

import { COMMON_CENTER } from './../../_centralized/_configuration/common-center';
import { GENERAL_VALUE } from './../../_centralized/_configuration/general-value';
import { PATH_PAGES } from './../../_centralized/_configuration/path-pages';

@Component({
  selector: 'cs-kps-delivery-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  private _readOnce: Subscription;

  statusSpinner: boolean;
  pageSizeOptions: number[];
  dataSource: MatTableDataSource<DetailOrderTable>;
  columnsToDisplay: string[];
  statusObject: any;
  keyTranslate: any;

  constructor(
    private router: Router,
    private manageLocalStorageService: ManageLocalStorageService,
    private realtimeDatabaseService: RealtimeDatabaseService
  ) { }

  ngOnInit() {
    this.pageSizeOptions = GENERAL_VALUE.PAGE_SIZE_OPTIONS;
    this.columnsToDisplay = ['invoiceNumber', 'dateTime', 'name', 'totalPrice', 'status'];
    this.keyTranslate = {
      invoiceNumber: 'SHARE.INVOICE_NUMBER',
      dateTime: 'SHARE.DATE',
      name: 'SHARE.NAME',
      totalPrice: 'SHARE.TOTAL_PRICE',
      status: 'SHARE.STATUS'
    };

    setTimeout(() => {
      this.statusSpinner = true;
      this._readOnce = this.realtimeDatabaseService.getStatus().subscribe((resultsStatus: any) => {
        this._readOnce.unsubscribe();
        const resultsValueStatus: any = resultsStatus.payload.val();
        console.log('..:: Status: ', resultsValueStatus);
        if (resultsValueStatus) {
          this.manageLocalStorageService.setStatus(JSON.stringify(resultsValueStatus));
          this.statusObject = JSON.parse(this.manageLocalStorageService.getStatus());
          this._readOnce = this.realtimeDatabaseService.getOrderItem().subscribe((resultsOrderItem: any) => {
            const resultsValueOrderItem: any = resultsOrderItem.payload.val();
            console.log('..:: Results Value Order Item: ', resultsValueOrderItem);
            if (resultsValueOrderItem) {
              const resultsKeyOrderItem: string[] = Object.keys(resultsValueOrderItem);
              resultsKeyOrderItem.reverse();
              const detailOrderTable: DetailOrderTable[] = [];
              for (const key of resultsKeyOrderItem) {
                detailOrderTable.push({
                  invoiceNumber: key,
                  dateTime: COMMON_CENTER.FORMATTED_DATE(new Date(Number(key))).replace(' ', '<br>'),
                  name: resultsValueOrderItem[key].sendContact.profile.name.replace(' ', '<br>'),
                  totalPrice: resultsValueOrderItem[key].totalPrice,
                  status: resultsValueOrderItem[key].status,
                  // status: this.statusObject[resultsValueOrderItem[key].status],
                  detailOrder: {
                    bankDetail: resultsValueOrderItem[key].bankDetail,
                    detailImage: resultsValueOrderItem[key].detailImage,
                    listOrder: resultsValueOrderItem[key].listOrder,
                    sendContact: resultsValueOrderItem[key].sendContact,
                    sendDetails: resultsValueOrderItem[key].sendDetails,
                    totalPrice: resultsValueOrderItem[key].totalPrice,
                    status: resultsValueOrderItem[key].status,
                  }
                });
              }
              console.log('..:: Detail Order Table: ', detailOrderTable);
              this.dataSource = new MatTableDataSource(detailOrderTable);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
            }
            this.statusSpinner = false;
          }, this.onServiceError.bind(this));
        } else {
          this.statusSpinner = false;
          this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
        }
      }, this.onServiceError.bind(this));
    }, 0);
  }

  ngOnDestroy(): void {
    this._readOnce.unsubscribe();
  }

  private onServiceError(error: any): void {
    this.statusSpinner = false;
    this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    console.log('..:: Service Error: ', error);
  }

  viewElement(element: DetailOrderTable): void {
    console.log('Detail Order: ', element);
    this.routerNavigateToHomeForm(element.invoiceNumber);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  routerNavigateToHomeForm(keyDetailOrder: string): void {
    const key: object = {};
    key[PATH_PAGES.QUERY_PARAMS.KEY] = keyDetailOrder;
    this.router.navigate(['/' + PATH_PAGES.AUTHENTICATE.ROOT + '/' + PATH_PAGES.AUTHENTICATE.HOME_FORM],
      {
        queryParams: key
      }
    );
  }

}
