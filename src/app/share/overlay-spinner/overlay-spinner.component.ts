import { Component } from '@angular/core';

@Component({
  selector: 'cs-kps-delivery-overlay-spinner',
  template: `
  <div class="overlay-spinner">
    <div class="content-spinner">
      <mat-spinner class="spinner-item"></mat-spinner>
      <p>{{ 'SHARE.PLEASE_WAIT' | translate }}</p>
    </div>
  </div>
  `,
  styles: [`
    .overlay-spinner {
      position: fixed;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: rgba(0, 0, 0, 0.3);
      z-index: 9999;
    }

    .content-spinner {
      text-align: center !important;
      margin-top: 40vh !important;
    }

    .spinner-item {
      margin: 0 auto !important;
    }
  `]
})
export class OverlaySpinnerComponent { }
