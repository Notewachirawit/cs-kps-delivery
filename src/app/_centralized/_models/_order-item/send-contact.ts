import { Profile } from './profile';

export interface SendContact {
    uidUser: string;
    profile: Profile;
}
