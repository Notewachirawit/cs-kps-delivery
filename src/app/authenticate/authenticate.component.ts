import { ActivatedRoute, Router } from '@angular/router';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { User } from 'firebase';

import { AuthenticationService } from '../_centralized/_services/authentication.service';
import { ManageLocalStorageService } from '../_centralized/_services/manage-local-storage.service';

import { SignOutDialogComponent } from './../share/sign-out-dialog/sign-out-dialog.component';

import { GENERAL_VALUE } from '../_centralized/_configuration/general-value';
import { KEY_LOCAL_STORAGE } from '../_centralized/_configuration/key-local-storage';
import { PATH_FILES } from '../_centralized/_configuration/path-files';
import { PATH_PAGES } from './../_centralized/_configuration/path-pages';
import { OBJECTS_ROUTER_VIEW } from './../_centralized/_configuration/objects-router-view';

@Component({
  selector: 'cs-kps-delivery-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.scss']
})
export class AuthenticateComponent implements OnInit {
  isHandset$: Observable<boolean>;
  statusSpinner: boolean;

  _PATH_FILES: any;
  _OBJECTS_ROUTER_VIEW: any;

  constructor(
    public matDialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private breakpointObserver: BreakpointObserver,
    private authenticationService: AuthenticationService,
    private manageLocalStorageService: ManageLocalStorageService) { }

  ngOnInit(): void {
    this._PATH_FILES = PATH_FILES.AUTHENTICATE.ROOT;
    this._OBJECTS_ROUTER_VIEW = OBJECTS_ROUTER_VIEW.AUTHENTICATE.ROOT;
    setTimeout(() => {
      this.isHandset$ = this.breakpointObserver.observe([
        Breakpoints.Small,
        Breakpoints.Handset
      ]).pipe(map((result: BreakpointState) => result.matches));
      this.activatedRoute.queryParams.subscribe((params: any) => {
        console.log('..:: queryParams: ', params);
        if (!(params[PATH_PAGES.QUERY_PARAMS.ROUTER]
          && PATH_PAGES.UNAUTHENTICATED.SIGN_IN === params[PATH_PAGES.QUERY_PARAMS.ROUTER])) {
          this.isNotRouterSignInPage();
        }
      });
    }, 0);
  }

  private isNotRouterSignInPage(): void {
    this.statusSpinner = true;
    const authState: Subscription = this.authenticationService.authState().subscribe((user: User) => {
      console.log('.. :: user: ', user);
      if (user && this.manageLocalStorageService.getUid()) {
        this.manageLocalStorageService.setAuthenticate(KEY_LOCAL_STORAGE.STATUS_AUTHENTICATE.Y);
        this.manageLocalStorageService.setUid(user.uid);
      } else {
        this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
      }
      this.statusSpinner = false;
      authState.unsubscribe();
    }, this.onServiceError.bind(this));
  }

  private onServiceError(error: any): void {
    this.statusSpinner = false;
    this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    console.log('..:: Service Error: ', error);
  }

  openSignOutDialog(): void {
    const signOutDialog: MatDialogRef<any, any> = this.matDialog.open(SignOutDialogComponent, {
      width: '300px',
      autoFocus: false
    });

    signOutDialog.afterClosed().subscribe((result: any) => {
      console.log('Sign out dialog was closed: ', result);
      if (result && GENERAL_VALUE.Y === result[PATH_PAGES.DIALOG_REF.VALUE]) {
        this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
      }
    }, this.onServiceError.bind(this));
  }

}
