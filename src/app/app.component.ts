import { AuthenticationService } from './_centralized/_services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { COMMON_CENTER } from './_centralized/_configuration/common-center';
import { PATH_PAGES } from './_centralized/_configuration/path-pages';

@Component({
  selector: 'cs-kps-delivery-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
    private translateService: TranslateService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    // localStorage.clear();
    this.languageManagement();
    this.authenticateManagement();
  }

  private languageManagement(): void {
    const keyLangs = Object.keys(COMMON_CENTER.LANGUAGE);
    const langs = [];
    for (let index = 0; index < keyLangs.length; index++) {
      langs.push(COMMON_CENTER.LANGUAGE[keyLangs[index]]);
    }
    this.translateService.addLangs(langs);
    this.translateService.setDefaultLang(COMMON_CENTER.LANGUAGE.TH);
    this.translateService.use(this.translateService.getDefaultLang());
    this.translateService.currentLang = this.translateService.getDefaultLang();
    console.log('..:: Current Language: ', this.translateService.currentLang);
  }

  private authenticateManagement(): void {
    console.log('..:: Is Authenticate: ', this.authenticationService.isAuthenticate());
    if (this.authenticationService.isAuthenticate()) {
      this.router.navigate([PATH_PAGES.AUTHENTICATE.ROOT], {});
    } else {
      this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    }
  }

}


