import { FIREBASE_PRODUCTS } from './../../_centralized/_configuration/firebase-products';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import {
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UploadTaskSnapshot } from '@angular/fire/storage/interfaces';

import { DetailFood } from './../../_centralized/_models/_food-item/detail-food';

import { CloudStorageService } from './../../_centralized/_services/cloud-storage.service';
import { RealtimeDatabaseService } from './../../_centralized/_services/realtime-database.service';

import { GENERAL_VALUE } from './../../_centralized/_configuration/general-value';
import { PATH_PAGES } from './../../_centralized/_configuration/path-pages';

@Component({
  selector: 'cs-kps-delivery-food-form',
  templateUrl: './food-form.component.html',
  styleUrls: ['./food-form.component.scss']
})
export class FoodFormComponent implements OnInit {

  @ViewChild('inputImageFile')
  inputImageFile: ElementRef;

  detailFoodForm: FormGroup;
  detailFood: DetailFood;
  statusSpinner: boolean;
  viewImage: string;
  outOfStock: boolean;
  acceptFiles: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private cloudStorageService: CloudStorageService,
    private realtimeDatabaseService: RealtimeDatabaseService
  ) { }

  ngOnInit(): void {
    this.detailFoodForm = this.formBuilder.group({
      name: [null, [Validators.required]],
      price: [null, [Validators.required, Validators.min(0)]],
      unit: [null, [Validators.required]],
      imageFile: [null, [Validators.required]]
    });
    this.acceptFiles = GENERAL_VALUE.ACCEPT_FILES.join(', ');
    this.viewImage = '';
    this.outOfStock = false;
    setTimeout(() => {
      this.statusSpinner = true;
      this.activatedRoute.queryParams.subscribe((params: any) => {
        console.log('..:: queryParams: ', params);
        if (params && '' !== params[PATH_PAGES.QUERY_PARAMS.KEY]) {
          const readOnce: Subscription = this.realtimeDatabaseService
            .getMenuItemByKey(params[PATH_PAGES.QUERY_PARAMS.KEY]).subscribe((resultsMenuItem: any) => {
              readOnce.unsubscribe();
              const resultsValueMenuItem: any = resultsMenuItem.payload.val();
              console.log('.. :: Results Value Menu: ', resultsValueMenuItem);
              if (resultsValueMenuItem) {
                this.detailFood = resultsValueMenuItem;
                this.detailFoodForm.patchValue({
                  name: params[PATH_PAGES.QUERY_PARAMS.KEY],
                  price: this.detailFood.price,
                  unit: this.detailFood.unit,
                  imageFile: this.detailFood.detailImage.path
                    .substring(this.detailFood.detailImage.path.lastIndexOf('/') + 1)
                });
                this.outOfStock = this.detailFood.outOfStock;
                console.log('this.outOfStock = ', this.outOfStock);
                this.viewImage = this.detailFood.detailImage.url;
                this.detailFoodForm.controls['name'].disable();
              }
              this.statusSpinner = false;
            }, this.onServiceError.bind(this));
        } else {
          this.statusSpinner = false;
        }
      });
    }, 0);
  }

  private onServiceError(error: any): void {
    this.statusSpinner = false;
    this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    console.log('..:: Service Error: ', error);
  }

  private saveMenuItem(pathFile: string, urlFile: string): void {
    this.detailFood = {
      price: this.detailFoodForm.value.price,
      unit: this.detailFoodForm.value.unit,
      useFlag: true,
      detailImage: {
        path: pathFile,
        url: urlFile
      },
      outOfStock: this.detailFood.outOfStock || true
    };
    const objectData: object = {};
    objectData[this.detailFoodForm.getRawValue().name] = this.detailFood;
    console.log('.. :: Object Data: ', objectData);
    this.realtimeDatabaseService.saveOrUpdateMenuItem(objectData);
    this.statusSpinner = false;
    this.routerNavigateToFood();
  }

  onFormSubmit(): void {
    console.log('.. :: Form Submit Valid: ', this.detailFoodForm);
    if (this.detailFoodForm.valid) {
      this.statusSpinner = true;
      const readOnce: Subscription = this.realtimeDatabaseService
        .getMenuItemByKey(this.detailFoodForm.getRawValue().name).subscribe((resultsMenuItem: any) => {
          readOnce.unsubscribe();
          const resultsValueMenuItem: any = resultsMenuItem.payload.val();
          console.log('.. :: Results Value Menu: ', resultsValueMenuItem);
          if (!resultsValueMenuItem || this.detailFood) {
            const blogFile: Blob = this.inputImageFile.nativeElement.files
              && 0 < this.inputImageFile.nativeElement.files.length ? this.inputImageFile.nativeElement.files[0] :
              undefined;
            if (blogFile) {
              const storageReference: AngularFireStorageReference = this.cloudStorageService
                .storageReference(FIREBASE_PRODUCTS.PATH_FILES.MENU_ITEM + `${this.detailFoodForm.value.imageFile}`);
              if (this.detailFood) {
                this.cloudStorageService.deleteFile(this.detailFood.detailImage.path);
              }
              const uploadTask: AngularFireUploadTask = this.cloudStorageService
                .uploadFile(FIREBASE_PRODUCTS.PATH_FILES.MENU_ITEM
                  + `${this.detailFoodForm.value.imageFile}`, blogFile);
              const subscribeUploadTask: Subscription = uploadTask.snapshotChanges().pipe(
                finalize(() => {
                  subscribeUploadTask.unsubscribe();
                  console.log('.. :: Transferred Success');
                  storageReference.getDownloadURL().subscribe((url: any) => {
                    this.saveMenuItem(FIREBASE_PRODUCTS.PATH_FILES.MENU_ITEM
                      + `${this.detailFoodForm.value.imageFile}`, url);
                  }, this.onServiceError.bind(this));
                })).subscribe((result: UploadTaskSnapshot) => {
                  console.log('.. :: Bytes Transferred: ', result.bytesTransferred);
                }, this.onServiceError.bind(this));
            } else {
              this.saveMenuItem(this.detailFood.detailImage.path, this.detailFood.detailImage.url);
            }
          } else {
            console.log('.. :: Data Duplicate :: ..');
            this.statusSpinner = false;
          }
        }, this.onServiceError.bind(this));
    }
  }

  deleteMenuItem(): void {
    this.statusSpinner = true;
    console.log('.. :: Delete Menu Item: ', this.detailFoodForm.getRawValue().name);
    const key: string = this.detailFoodForm.getRawValue().name;
    this.realtimeDatabaseService.deleteMenuItem(key).then(() => {
      this.cloudStorageService.deleteFile(this.detailFood.detailImage.path);
      this.statusSpinner = false;
      this.routerNavigateToFood();
    }).catch(this.onServiceError.bind(this));
  }

  inputImageFileChange(fileList: FileList): void {
    console.log('.. :: File List: ', fileList);
    if (fileList && 0 < fileList.length) {
      const name: string[] = GENERAL_VALUE.ACCEPT_FILES
        .filter(acceptItem => {
          return acceptItem.includes(fileList[0].name.substring(fileList[0].name.lastIndexOf('.')));
        });
      if (0 < name.length) {
        this.detailFoodForm.patchValue({
          imageFile: fileList[0].name
        });
        const fileReader: FileReader = new FileReader();
        fileReader.readAsDataURL(fileList[0]);
        fileReader.onload = (): void => {
          this.viewImage = fileReader.result.toString();
        };
      } else {
        this.detailFoodForm.patchValue({
          imageFile: null
        });
        this.viewImage = '';
      }
    } else {
      this.detailFoodForm.patchValue({
        imageFile: null
      });
      this.viewImage = '';
    }
  }

  changeStatusBuy(status: boolean): void {
    this.detailFood.outOfStock = !status;
    const objectData: object = {};
    objectData[this.detailFoodForm.getRawValue().name] = this.detailFood;
    console.log('.. :: Object Data: ', objectData);
    this.realtimeDatabaseService.saveOrUpdateMenuItem(objectData);
    this.statusSpinner = false;
    this.routerNavigateToFood();
  }

  routerNavigateToFood(): void {
    this.detailFoodForm.reset();
    this.router.navigate(['/' + PATH_PAGES.AUTHENTICATE.ROOT + '/' + PATH_PAGES.AUTHENTICATE.FOOD], {});
  }

}
