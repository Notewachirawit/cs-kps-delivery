import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnauthenticatedComponent } from './unauthenticated.component';
import { SignInComponent } from './sign-in/sign-in.component';

import { PATH_PAGES } from './../_centralized/_configuration/path-pages';

const routes: Routes = [
  {
    path: PATH_PAGES.UNAUTHENTICATED.ROOT,
    component: UnauthenticatedComponent,
    children: [
      {
        path: PATH_PAGES.UNAUTHENTICATED.SIGN_IN,
        component: SignInComponent
      },
      {
        path: '',
        redirectTo: PATH_PAGES.UNAUTHENTICATED.SIGN_IN,
        pathMatch: PATH_PAGES.GENERAL.FULL
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnauthenticatedRoutingModule { }
