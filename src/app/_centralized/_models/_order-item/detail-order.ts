import { ListOrder } from './list-order';
import { SendContact } from './send-contact';
import { DetailImage } from './detail-image';
import { SendDetails } from './send-details';
import { Bank } from './bank';

export interface DetailOrder {
    bankDetail: Bank;
    detailImage: DetailImage;
    listOrder: ListOrder[];
    sendContact: SendContact;
    sendDetails: SendDetails;
    totalPrice: number;
    status: string;
}
