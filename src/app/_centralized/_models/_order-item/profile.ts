export interface Profile {
    username: string;
    name: string;
    address: string;
    telephone: string;
    userType: string;
}
