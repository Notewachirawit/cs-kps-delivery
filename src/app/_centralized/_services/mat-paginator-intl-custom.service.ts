import { Injectable } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { MatPaginatorIntl } from '@angular/material';

@Injectable()
export class MatPaginatorIntlCustomService extends MatPaginatorIntl {

    private _ITEMS_PER_PAGE: string;
    private _OF: string;

    constructor(
        private translateService: TranslateService
    ) {
        super();
        this.onInit();
    }

    private onInit(): void {
        this._ITEMS_PER_PAGE = 'SHARE.ITEMS_PER_PAGE';
        this._OF = 'SHARE.OF';
        this.itemsPerPageLabel = '';
        // this.nextPageLabel = ' My new label for next page';
        // this.previousPageLabel = ' My new label for previous page';
        console.log('.. :: Mat Paginator IntlCro Service Init: ', this.translateService.currentLang);
        this.translateService.get([this._ITEMS_PER_PAGE, this._OF]).subscribe(this.callbackGetKeyTranslate.bind(this));
        this.translateService.onLangChange.subscribe((langChangeEvent: LangChangeEvent) => {
            console.log('.. :: Mat Paginator IntlCro Service Lang Change Event: ', langChangeEvent);
            this.translateService.get([this._ITEMS_PER_PAGE, this._OF]).subscribe(this.callbackGetKeyTranslate.bind(this));
        });
    }

    private callbackGetKeyTranslate(result: string[]): void {
        this.itemsPerPageLabel = result[this._ITEMS_PER_PAGE];
        this.getRangeLabel = (page: number, pageSize: number, length: number): string => {
            if (length === 0 || pageSize === 0) {
                return `0 ${result[this._OF]} ${length}`;
            }
            length = Math.max(length, 0);
            const startIndex = page * pageSize;
            const endIndex = startIndex < length
                ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
            return `${startIndex + 1} - ${endIndex} ${result[this._OF]} ${length}`;
        };
    }

}
