import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../_centralized/_services/authentication.service';
import { ManageLocalStorageService } from '../_centralized/_services/manage-local-storage.service';

import { KEY_LOCAL_STORAGE } from '../_centralized/_configuration/key-local-storage';

@Component({
  selector: 'cs-kps-delivery-unauthenticated',
  templateUrl: './unauthenticated.component.html',
  styleUrls: ['./unauthenticated.component.scss']
})
export class UnauthenticatedComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private manageLocalStorageService: ManageLocalStorageService
  ) { }

  ngOnInit(): void {
    console.log('..:: Unauthenticated Component Execute');
    this.authenticationService.signOut().then(() => {
      this.manageLocalStorageService.removeStatus();
      this.manageLocalStorageService.removeUid();
      this.manageLocalStorageService.setAuthenticate(KEY_LOCAL_STORAGE.STATUS_AUTHENTICATE.N);
    }).catch(this.onServiceError.bind(this));
  }

  private onServiceError(error: any): void {
    this.manageLocalStorageService.removeStatus();
    this.manageLocalStorageService.removeUid();
    this.manageLocalStorageService.setAuthenticate(KEY_LOCAL_STORAGE.STATUS_AUTHENTICATE.N);
    console.log('..:: Service Error: ', error);
  }

}
