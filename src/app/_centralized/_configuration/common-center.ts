import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader } from '@ngx-translate/core';

import { PATH_FILES } from './path-files';


export const HttpLoaderFactory: Function = (httpClient: HttpClient): TranslateHttpLoader => {
    return new TranslateHttpLoader(httpClient,
        PATH_FILES.SHARE.JSON.LANGUAGE.PREFIX,
        PATH_FILES.SHARE.JSON.LANGUAGE.SUFFIX);
};

export function FormatterDate(date: Date) {
    let day: any = date.getDate();
    let month: any = date.getMonth() + 1;
    const year: any = date.getFullYear();
    let hours: any = date.getHours();
    let minutes: any = date.getMinutes();
    let seconds: any = date.getSeconds();
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }
    if (hours < 10) {
        hours = '0' + hours;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    if (seconds < 10) {
        seconds = '0' + seconds;
    }
    return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes + ':' + seconds;
}

export function FormatterDateOnly(date: Date) {
    let day: any = date.getDate();
    let month: any = date.getMonth() + 1;
    const year: any = date.getFullYear();
    if (day < 10) {
        day = '0' + day;
    }
    if (month < 10) {
        month = '0' + month;
    }
    return day + '/' + month + '/' + year;
}

export const COMMON_CENTER: any = {
    TRANSLATE_FUNCTION: {
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        },
        isolate: false
    },
    LANGUAGE: {
        TH: 'th'
    },
    SUFFIXES_USERNAME: '@cs.ku.kps',
    FORMATTED_DATE: FormatterDate,
    FORMATTED_DATE_ONLY: FormatterDateOnly
};
