import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { RealtimeDatabaseService } from './../../_centralized/_services/realtime-database.service';

import { DetailOrderTable } from './../../_centralized/_models/_item-table/detail-order-table';

import { COMMON_CENTER } from './../../_centralized/_configuration/common-center';
import { GENERAL_VALUE } from './../../_centralized/_configuration/general-value';
import { PATH_PAGES } from './../../_centralized/_configuration/path-pages';

@Component({
  selector: 'cs-kps-delivery-selling',
  templateUrl: './selling.component.html',
  styleUrls: ['./selling.component.scss']
})
export class SellingComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  private _readOnce: Subscription;

  pageSizeOptions: number[];
  dataSource: MatTableDataSource<DetailOrderTable>;
  columnsToDisplay: string[];
  nameListMenu: string[];
  totalPrice: number;
  totalPriceToDay: number;
  statusSpinner: boolean;
  statusObject: any;
  keyTranslate: any;
  tempListMenuBackToTopDate: any;
  tempListMenuToDate: any;
  messageToDay: string;
  messageToTopDate: string;

  constructor(
    private router: Router,
    private realtimeDatabaseService: RealtimeDatabaseService
  ) { }

  ngOnInit() {
    this.pageSizeOptions = GENERAL_VALUE.PAGE_SIZE_OPTIONS;
    this.columnsToDisplay = ['invoiceNumber', 'dateTime', 'name', 'totalPrice', 'status'];
    this.keyTranslate = {
      invoiceNumber: 'SHARE.INVOICE_NUMBER',
      dateTime: 'SHARE.DATE',
      name: 'SHARE.NAME',
      totalPrice: 'SHARE.TOTAL_PRICE',
      status: 'SHARE.STATUS'
    };
    setTimeout(() => {
      this.statusSpinner = true;
      const readOnce: Subscription = this.realtimeDatabaseService.getMenuItem().subscribe((resultsMenuItem: any) => {
        readOnce.unsubscribe();
        const resultsValueMenuItem: any = resultsMenuItem.payload.val();
        console.log('..:: Results Value Menu Item: ', resultsValueMenuItem);
        if (resultsValueMenuItem) {
          const resultsKeyFoodItem: string[] = Object.keys(resultsValueMenuItem);
          this._readOnce = this.realtimeDatabaseService.getOrderItem().subscribe((resultsOrderItem: any) => {
            const resultsValueOrderItem: any = resultsOrderItem.payload.val();
            console.log('..:: Results Value Order Item: ', resultsValueOrderItem);
            this.tempListMenuBackToTopDate = {};
            this.tempListMenuToDate = {};
            this.totalPrice = 0;
            this.totalPriceToDay = 0;
            if (resultsValueOrderItem) {
              const resultsKeyOrderItem: string[] = Object.keys(resultsValueOrderItem);
              resultsKeyOrderItem.reverse();
              this.nameListMenu = [];
              for (const keyFood of resultsKeyFoodItem) {
                this.nameListMenu.push(keyFood);
                this.tempListMenuBackToTopDate[keyFood] = {
                  sell: 0,
                  amount: 0
                };
                this.tempListMenuToDate[keyFood] = {
                  sell: 0,
                  amount: 0
                };
              }

              const backToTopDate: Date = new Date();
              backToTopDate.setHours(0);
              backToTopDate.setMinutes(0);
              backToTopDate.setSeconds(0);
              backToTopDate.setMilliseconds(0);
              backToTopDate.setDate(backToTopDate.getDate() - 7);
              const toDate: Date = new Date();
              toDate.setHours(0);
              toDate.setMinutes(0);
              toDate.setSeconds(0);
              toDate.setMilliseconds(0);
              this.messageToDay = COMMON_CENTER.FORMATTED_DATE_ONLY(toDate);
              this.messageToTopDate = COMMON_CENTER.FORMATTED_DATE_ONLY(backToTopDate);
              for (const keyOrder of resultsKeyOrderItem) {
                if (resultsValueOrderItem[keyOrder].listOrder) {
                  for (const listOrder of resultsValueOrderItem[keyOrder].listOrder) {
                    if (this.tempListMenuBackToTopDate[listOrder.name]) {
                      if (backToTopDate.getTime() < Number(keyOrder)) {
                        this.tempListMenuBackToTopDate[listOrder.name].sell += Number(listOrder.amountPrice);
                        this.tempListMenuBackToTopDate[listOrder.name].amount += Number(listOrder.amount);
                      }
                      if (toDate.getTime() < Number(keyOrder)) {
                        this.tempListMenuToDate[listOrder.name].sell += Number(listOrder.amountPrice);
                        this.tempListMenuToDate[listOrder.name].amount += Number(listOrder.amount);
                      }
                    }
                  }
                  if (backToTopDate.getTime() < Number(keyOrder)) {
                    this.totalPrice += resultsValueOrderItem[keyOrder].totalPrice;
                  }
                  if (toDate.getTime() < Number(keyOrder)) {
                    this.totalPriceToDay += resultsValueOrderItem[keyOrder].totalPrice;
                  }
                }
              }
            }
            this.statusSpinner = false;
          }, this.onServiceError.bind(this));
        } else {
          this.statusSpinner = false;
        }
      }, this.onServiceError.bind(this));
    }, 0);
  }

  ngOnDestroy(): void {
    this._readOnce.unsubscribe();
  }

  private onServiceError(error: any): void {
    this.statusSpinner = false;
    this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    console.log('..:: Service Error: ', error);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
