export const KEY_LOCAL_STORAGE: any = {
    AUTHENTICATE: 'AUTHENTICATE',
    STATUS_AUTHENTICATE: {
        Y: 'Y',
        N: 'N'
    },
    STATUS: 'STATUS',
    UID: 'UID'
};
