export const PATH_FILES: any = {
  SHARE: {
    JSON: {
      LANGUAGE: {
        PREFIX: './assets/i18n/language/',
        SUFFIX: '.json'
      }
    }
  },
  UNAUTHENTICATED: {
    SIGN_IN: {
      ADMINISTRATION: {
        SRC: './assets/images/unauthenticated/sign-in/computer.png',
        ALT: ''
      }
    }
  },
  AUTHENTICATE: {
    ROOT: {
      TITLE_SLIDE: {
        SRC: './assets/images/authenticate/root/postman.png',
        ALT: ''
      }
    }
  }
};
