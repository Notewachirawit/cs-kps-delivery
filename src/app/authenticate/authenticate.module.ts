import { NgModule } from '@angular/core';

import { AuthenticateRoutingModule } from './authenticate-routing.module';
import { ShareModule } from '../share/share.module';

import { AuthenticateComponent } from './authenticate.component';
import { HomeComponent } from './home/home.component';
import { SellingComponent } from './selling/selling.component';
import { FoodComponent } from './food/food.component';
import { StaffComponent } from './staff/staff.component';
import { FoodFormComponent } from './food-form/food-form.component';
import { HomeFormComponent } from './home-form/home-form.component';
import { StaffFormComponent } from './staff-form/staff-form.component';

@NgModule({
  declarations: [
    AuthenticateComponent,
    HomeComponent,
    SellingComponent,
    FoodComponent,
    StaffComponent,
    FoodFormComponent,
    HomeFormComponent,
    StaffFormComponent
  ],
  imports: [
    ShareModule,
    AuthenticateRoutingModule
  ]
})
export class AuthenticateModule { }
