import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';

import { ManageLocalStorageService } from './../../_centralized/_services/manage-local-storage.service';
import { RealtimeDatabaseService } from './../../_centralized/_services/realtime-database.service';

import { DetailOrder } from './../../_centralized/_models/_order-item/detail-order';

import { GENERAL_VALUE } from './../../_centralized/_configuration/general-value';
import { PATH_PAGES } from './../../_centralized/_configuration/path-pages';

@Component({
  selector: 'cs-kps-delivery-home-form',
  templateUrl: './home-form.component.html',
  styleUrls: ['./home-form.component.scss']
})
export class HomeFormComponent implements OnInit, OnDestroy {

  _readOnce: Subscription;
  detailOrder: DetailOrder;
  shippingSupervisorFormControl: FormControl;
  filteredOptions: Observable<string[]>;
  detailUsers: string[];
  resultKeyUsers: string[];
  uids: object;

  statusSpinner: boolean;
  invoiceNumber: string;
  statusSelected: string;
  statusItems: any[];
  getStatus: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private manageLocalStorageService: ManageLocalStorageService,
    private realtimeDatabaseService: RealtimeDatabaseService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.shippingSupervisorFormControl = new FormControl();
      this.uids = {};
      this.detailUsers = [];
      this.filteredOptions = this.shippingSupervisorFormControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this.applyFilter(value))
        );
      this.statusSpinner = true;
      this.getStatus = JSON.parse(this.manageLocalStorageService.getStatus());
      const keyGetStatus = Object.keys(this.getStatus);
      this.statusItems = [];
      for (const key of keyGetStatus) {
        this.statusItems.push(this.getStatus[key]);
      }
      console.log('.. :: Status Items: ', this.statusItems);
      this.activatedRoute.queryParams.subscribe((params: any) => {
        console.log('..:: queryParams: ', params);
        if (params && '' !== params[PATH_PAGES.QUERY_PARAMS.KEY]) {
          this.invoiceNumber = params[PATH_PAGES.QUERY_PARAMS.KEY];
          const readOnce: Subscription = this.realtimeDatabaseService.getUsers().subscribe((resultsUsers: any) => {
            readOnce.unsubscribe();
            const resultsValueUsers: any = resultsUsers.payload.val();
            console.log('..:: Results Value Users: ', resultsValueUsers);
            if (resultsValueUsers) {
              const resultKeyUsers: string[] = Object.keys(resultsValueUsers);
              this._readOnce = this.realtimeDatabaseService.getOrderItemByKey(this.invoiceNumber).subscribe((resultsOrderItem: any) => {
                const resultsValueOrderItem: any = resultsOrderItem.payload.val();
                console.log('.. :: Results Value Order: ', resultsValueOrderItem);
                if (resultsValueOrderItem) {
                  this.detailOrder = resultsValueOrderItem;
                  this.statusSelected = this.detailOrder.status;
                  for (const key of resultKeyUsers) {
                    if ('S' === resultsValueUsers[key].profile.userType) {
                      this.uids[`${resultsValueUsers[key].profile.name}: ${resultsValueUsers[key].profile.username}`] = key;
                      this.detailUsers.push(`${resultsValueUsers[key].profile.name}: ${resultsValueUsers[key].profile.username}`);
                      if (key === this.detailOrder.sendDetails.staff) {
                        this.shippingSupervisorFormControl
                          .patchValue(`${resultsValueUsers[key].profile.name}: ${resultsValueUsers[key].profile.username}`);
                      }
                    }
                  }
                } else {
                  this.statusSpinner = false;
                }
              }, this.onServiceError.bind(this));
            }
            this.statusSpinner = false;
          }, this.onServiceError.bind(this));

        } else {
          this.statusSpinner = false;
        }
      });
    }, 0);
  }

  private onServiceError(error: any): void {
    this.statusSpinner = false;
    this.router.navigate([PATH_PAGES.UNAUTHENTICATED.ROOT], {});
    console.log('..:: Service Error: ', error);
  }

  ngOnDestroy(): void {
    this._readOnce.unsubscribe();
  }

  applyFilter(value: string) {
    return this.detailUsers.filter((optionUsers: string) => optionUsers.toLowerCase().includes(value.toLowerCase()));
  }

  statusSelectedChange(value: string): void {
    console.log('.. :: value: ', value);
    const objectOrderItem: object = {};
    objectOrderItem[this.invoiceNumber] = {
      // listOrder: this.detailOrder.listOrder,
      // sendContact: this.detailOrder.sendContact,
      // staff: this.detailOrder.staff,
      // status: value,
      // totalPrice: this.detailOrder.totalPrice
      bankDetail: this.detailOrder.bankDetail,
      detailImage: this.detailOrder.detailImage,
      listOrder: this.detailOrder.listOrder,
      sendContact: this.detailOrder.sendContact,
      sendDetails: this.detailOrder.sendDetails,
      totalPrice: this.detailOrder.totalPrice,
      status: value,
    };
    this.realtimeDatabaseService.saveOrUpdateOrderItem(objectOrderItem).then(() => {
      console.log('Update Order Item Success: ', objectOrderItem);
    }).catch(this.onServiceError.bind(this));
  }

  selectStaff(value: string): void {
    console.log('.. :: Value: ', value);
    console.log('.. :: UID: ', this.uids[value]);
    if (this.uids[value]) {
      const objectOrderItem: object = {};
      objectOrderItem[this.invoiceNumber] = {
        // listOrder: this.detailOrder.listOrder,
        // sendContact: this.detailOrder.sendContact,
        // staff: this.uids[value],
        // status: this.getStatus[GENERAL_VALUE.OP],
        // totalPrice: this.detailOrder.totalPrice

        bankDetail: this.detailOrder.bankDetail,
        detailImage: this.detailOrder.detailImage,
        listOrder: this.detailOrder.listOrder,
        sendContact: this.detailOrder.sendContact,
        sendDetails: {
          rateFlag: this.detailOrder.sendDetails.rateFlag,
          rateMsg: this.detailOrder.sendDetails.rateMsg,
          staff: this.uids[value]
        },
        totalPrice: this.detailOrder.totalPrice,
        status: this.getStatus[GENERAL_VALUE.OP],
      };
      this.realtimeDatabaseService.saveOrUpdateOrderItem(objectOrderItem).then(() => {
        console.log('Update Order Item Success: ', objectOrderItem);
      }).catch(this.onServiceError.bind(this));
    }
  }


}
