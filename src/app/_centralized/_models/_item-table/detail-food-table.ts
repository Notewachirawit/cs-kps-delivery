import { DetailFood } from '../_food-item/detail-food';

export interface DetailFoodTable {
    name: string;
    price: number;
    unit: string;
    imageFile: string;
    detailFood: DetailFood;
    status: string;
}
