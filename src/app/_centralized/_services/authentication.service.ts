import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { ManageLocalStorageService } from './manage-local-storage.service';
import { Observable } from 'rxjs';

import { User } from 'firebase';

import { COMMON_CENTER } from '../_configuration/common-center';
import { KEY_LOCAL_STORAGE } from './../_configuration/key-local-storage';

@Injectable()
export class AuthenticationService {

  constructor(
    private angularFireAuth: AngularFireAuth,
    private manageLocalStorageService: ManageLocalStorageService
  ) { }

  isAuthenticate(): boolean {
    return KEY_LOCAL_STORAGE.STATUS_AUTHENTICATE.Y === this.manageLocalStorageService.getAuthenticate();
  }

  createUserWithUsernameAndPassword(username: string, password: string): Promise<firebase.auth.UserCredential> {
    username += COMMON_CENTER.SUFFIXES_USERNAME;
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(username, password);
  }

  signInWithUsernameAndPassword(username: string, password: string): Promise<firebase.auth.UserCredential> {
    username += COMMON_CENTER.SUFFIXES_USERNAME;
    return this.angularFireAuth.auth.signInWithEmailAndPassword(username, password);
  }

  authState(): Observable<User> {
    return this.angularFireAuth.authState;
  }

  signOut(): Promise<void> {
    return this.angularFireAuth.auth.signOut();
  }

}
