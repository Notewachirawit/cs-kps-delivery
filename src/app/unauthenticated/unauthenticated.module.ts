import { NgModule } from '@angular/core';

import { UnauthenticatedRoutingModule } from './unauthenticated-routing.module';
import { ShareModule } from '../share/share.module';

import { UnauthenticatedComponent } from './unauthenticated.component';
import { SignInComponent } from './sign-in/sign-in.component';

@NgModule({
  declarations: [UnauthenticatedComponent, SignInComponent],
  imports: [
    ShareModule,
    UnauthenticatedRoutingModule
  ]
})
export class UnauthenticatedModule { }
